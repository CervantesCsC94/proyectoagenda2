﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Agenda.Models;

namespace Proyecto_Agenda.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home

        PCesarEntities ConexionBD = new PCesarEntities();



        public ActionResult Index()
        {
            if (Session["usuario_id"]!=null)
            {
                List<Contacto> listaContactos = ConexionBD.Contactoes.ToList();
                return View(listaContactos);
            }
            else
            {
                return RedirectToAction("LogIn", "Sesion");
            }
        }


            
        public ActionResult Edit(int id)
        {
            return View();
        }

       
        [HttpPost]
        public ActionResult Edit(Contacto ContactosEditado)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

       [HttpPost]
        public ActionResult Create(Contacto contacto)
        {
            return View();
        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
        }


    }
}