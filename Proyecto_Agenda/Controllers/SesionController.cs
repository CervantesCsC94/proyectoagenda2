﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Agenda.Models;

namespace Proyecto_Agenda.Controllers
{
    public class SesionController : Controller
    {

        PCesarEntities ConexionBD = new PCesarEntities();


        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }//LogIn


        
        [HttpPost]
        public ActionResult LogIn(LogIn usuario_session)
        {
            if (ModelState.IsValid)
            {

                var usuarioConectado = ConexionBD.Usuarios.Where(usuario => usuario.Correo == usuario_session.correo && usuario.Pass == usuario_session.password).SingleOrDefault();

                if (usuarioConectado != null)
                {
                    Session["usuario_id"] = usuarioConectado.Id;
                    Session["usuario_Nombre"] = usuarioConectado.Nombre;

                    return RedirectToAction("Index","Home");
                }//if

            }//if


            return View();
        }//LogIn
    }
}