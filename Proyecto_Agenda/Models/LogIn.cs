﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Agenda.Models
{
    public class LogIn
    {

        [Required(ErrorMessage = "Es necesario que ingrese un correo Electronico")]
        [DisplayName("Correo")]
        public string correo { get; set; }


        [Required(ErrorMessage = "Es necesario que ingrese una contraseña")]
        [DisplayName("Contraseña")]
        public string password { get; set; }

    }
}